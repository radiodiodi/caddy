FROM abiosoft/caddy

COPY run_caddy.sh ./

ENTRYPOINT ["/bin/sh", "run_caddy.sh"]

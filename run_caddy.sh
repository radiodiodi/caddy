#!/bin/sh

echo "CADDY_CONFIG:"
echo "$CADDY_CONFIG" | tee /etc/Caddyfile

/bin/parent caddy -conf /etc/Caddyfile --log stdout -agree -email radiodiodi@radiodiodi.fi "$@"
